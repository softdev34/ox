
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Admin
 */
public class testOX {

    public static void main(String[] args) {
        OX ox = new OX();

        Scanner kb = new Scanner(System.in);
        int round = 1;
        int row = 0;
        int col = 0;
        String x = null;

        System.out.println("Welcome to OX Game");

        while (round >= 0) {
            ox.tableOX();
            if (round == 1) {
                System.out.println("Turn O");
                x = "O";
                round++;
            } else if (round == 2) {
                System.out.println("Turn X");
                x = "X";
                round--;
            }

            while (true) {
                System.out.println("Please input row, col: ");
                row = kb.nextInt();
                col = kb.nextInt();
                System.out.println();

                break;
            }
            ox.table[row - 1][col - 1] = x;

            if (ox.Winnable(ox.table).equals("X")) {
                ox.tableOX();
                System.out.println(" ");
                System.out.println(">>>X Win<<<");
                round = -1;
            } else if (ox.Winnable(ox.table).equals("O")) {
                ox.tableOX();
                System.out.println(" ");
                System.out.println(">>>O Win<<<");
                round = -1;
            } else {
                if (ox.Full(ox.table)) {
                    ox.tableOX();
                    System.out.println(" ");
                    System.out.println(">>>Draw<<");
                    round = -1;
                }
            }
        }

    }
}
