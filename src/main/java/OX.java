/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Admin
 */
public class OX {

    String[][] table = {
        {"-", "-", "-"},
        {"-", "-", "-"},
        {"-", "-", "-"}
    };

    public void tableOX() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print(table[i][j] + " ");
            }
            System.out.println("");

        }

    }

    public String Winnable(String[][] table) {
        for (int i = 0; i < 3; i++) {
            if (table[i][0] == table[i][1] && table[i][1] == table[i][2]
                    && table[i][0] != "-") {

                return table[i][0];
            }
        }

        for (int j = 0; j < 3; j++) {
            if (table[0][j] == table[1][j] && table[1][j] == table[2][j]
                    && table[0][j] != "-") {

                return table[0][j];
            }
        }

        if (table[0][0] == table[1][1] && table[1][1] == table[2][2]
                && table[0][0] != "-") {

            return table[0][0];
        }
        if (table[2][0] == table[1][1] && table[1][1] == table[0][2]
                && table[2][0] != "-") {

            return table[2][0];
        }

        return " ";

    }

    public boolean Full(String[][] table) {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (table[i][j] == "-") {
                    return false;
                }
            }
        }
        return true;
    }

}
